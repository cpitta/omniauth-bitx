require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Bitx < OmniAuth::Strategies::OAuth2
      option :name, 'bitx'
      option :client_options, {
          :site => 'https://www.bitx.co',
          :proxy => ENV['http_proxy'] ? URI(ENV['http_proxy']) : nil, 
          :token_url   => 'https://api.mybitx.com/api/oauth2/grant',
          :authorize_url => 'https://www.bitx.co/oauth2/authorize',
          :raise_errors => false
      }

      uid { raw_info['email'] }

      info do
        { 
            :name => raw_info['first_name'],
            :email => raw_info['email']
        }
      end

      extra do
        { :raw_info => raw_info }
      end

      def raw_info
        @raw_info ||= MultiJson.load(access_token.get('/api/v1/users/me').body)
      rescue ::Errno::ETIMEDOUT
        raise ::Timeout::Error
      end
      
      def build_access_token
        
        code = request.params["code"]
        params = {'grant_type' => 'authorization_code', 'code' => code}
        opts = {}
        
        opts[:body] = params
        opts[:headers] =  {'Content-Type' => 'application/x-www-form-urlencoded', 'Authorization' => basic_auth_header}
        
        response = client.connection.run_request(:post, options.client_options.token_url, opts[:body], opts[:headers]) 
        if response.status == 200
          response_parsed = JSON.parse response.body
        end
        verifier = request.params['code']
        response_parsed['provider']='bitx'
        response_parsed
      end

    def callback_phase
      env['omniauth.auth'] = build_access_token
      call_app!
    end



      def basic_auth_header
        "Basic " + Base64.strict_encode64("#{options[:client_id]}:#{options[:client_secret]}")
      end

    end
  end
end