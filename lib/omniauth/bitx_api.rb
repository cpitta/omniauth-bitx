module BitxAPI

  class Client 
   BASE_URI = 'https://api.mybitx.com/api/1/'

    
  def initialize(apikey, apisecret)
    @apikey = apikey
    @apisecret = apisecret 
  end
  

 
  def post(path,params)
    uri = URI(BASE_URI+path)
    req = Net::HTTP::Post.new(uri)
    req.basic_auth @apikey, @apisecret
    req.set_form_data(params)
    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http|
    http.request(req)
    }
    JSON.parse(res.body)    
  end
  
  def get(path)
    
    uri = URI(BASE_URI+path)
    req = Net::HTTP::Get.new(uri)
    req.basic_auth @apikey, @apisecret
    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http|
    http.request(req)
    }
    JSON.parse(res.body)

  end
  
  
  def balance
    get('balance')
  end
  
  def send(amount, address)
    post('send',{'amount'=>amount, 'address'=> address})
  end
  
    
  end
  
end