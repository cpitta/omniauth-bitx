# OmniAuth Btctc

This gem is an OmniAuth 2.0 Strategy for the [bitx API](http://bitx.Btctc.com/)
 

## Usage

Add the strategy to your `Gemfile` alongside OmniAuth:

```ruby
gem 'omniauth'
gem 'omniauth-bitx'
```

You will have to put in your consumer key and secret (Btctc refers to them as Key and Shared Secret).

For additional information, refer to the [OmniAuth wiki](https://github.com/intridea/omniauth/wiki).


## Contributing

* Fork the project.
* Make your feature addition or bug fix.
* Add tests for it. This is important so I don't break it in a
  future version unintentionally.
* Commit, do not mess with rakefile, version, or history.
  (if you want to have your own version, that is fine but bump version in a commit by itself I can ignore when I pull)
* Send me a pull request. Bonus points for topic branches.
